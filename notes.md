<a name="1.0.1.20161026"></a>
# 1.0.1.20161026 "Build" (2016-10-28)
#### Download 
 [Linux](https://gitlab.com)


### Data Source Explorer

* _Fix_: US102: Fix Generate DDL, add UI tests ([!84](https://gitlab.com/gzunino/dbt2/merge_requests/84))
* _Fix_: US103: Fix exceptions on Add/Edit Schema on DSE ([!86](https://gitlab.com/gzunino/dbt2/merge_requests/86))

