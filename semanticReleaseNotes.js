//var JSONStream = require('JSONStream')
var fs = require('fs')
var fromArray = require('from2-array')
var conventionalCommitsParser = require('conventional-commits-parser')
var conventionalChangelogWriter = require('conventional-changelog-writer')
var readFileSync = require('fs').readFileSync
var join = require('path').join
var _ = require('lodash')

exports.build = (mergeRequests, ctx) => {
    var options = {
        warn: true
    }
    var writerOpts = {
        //includeDetails: true,
        //debug: (d) => {console.log('debug:', d)}
        mainTemplate: readFileSync(join(__dirname, 'group_template.hbs'), 'utf-8'),
        commitPartial: readFileSync(join(__dirname, 'commit.hbs'), 'utf-8'),
        groupBy: 'scope',
        commitGroupsSort: 'title'/*, 
        transform: function(commit) {
            commit.version = '1.0.0';
            return commit;
          }*/
    }
    var context = _.extend({
        //title: 'Rls Title',
        //version: '2.1.3',
        //repository: 'gzunino/dbt2',
        host: 'https://gitlab.com',
        issue: 'issues',
        commit: 'merge_requests'
    }, ctx)

    var semanticMRs = mergeRequests.filter(internals).map(mrToSemanticMsg)
  
    fromArray.obj(semanticMRs)
    .on('error', (err) => {
        console.error(err);
        process.exit(1);
    })
    .pipe(conventionalCommitsParser(options))
    //.pipe(JSONStream.stringify())
    .pipe(conventionalChangelogWriter(context, writerOpts))
    //.pipe(process.stdout)
    .pipe(fs.createWriteStream('./notes.md'))
    console.log('written notes.md')
}

function mrToSemanticMsg(mr) {
    var type = exports.getType(mr)
    var scope = exports.getScope(mr)
    var semanticCommit = `${type}(${scope}): ${mr.title}

${mr.description}

-hash-
${mr.iid}`
    return semanticCommit
}

exports.getType = (mr) => {
    var typeLabels = mr.labels
        .filter((l) => l.indexOf('#') == 0)
    var type = typeLabels[0] || ''
    return type.replace('#', '')
}

exports.getScope = (mr) => {
    var typeLabels = mr.labels
        .filter((l) => l.indexOf('#') == -1)
    var scope = typeLabels[0] || ''
    return scope
}

function internals(mr) {
    return mr.labels.indexOf("#internal") == -1;
} 