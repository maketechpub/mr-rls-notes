#!/usr/bin/env node

var Promise = require('bluebird')
var git = require('gift')
var Gitlab = require('gitlab/dist/es5').default
var semanticReleaseNotes = require('./semanticReleaseNotes')
var argv = require('yargs')
    .default('git_repoPath', process.cwd())
    .default('tags', "[a-zA-Z]{1,5}[-_/].*")
    .demand(['gitlab_projectId', 'gitlab_token', 'branch'])
    .config('context', 'Pass context to "conventional-changelog-writer"')
    .help('help')
    .argv

var repo = null
var tagCommits = {}
var currentBranch = argv.branch
var tagRegex = argv.tags

function getMergeRequests() {
    var client = new Gitlab({
        token: argv.gitlab_token,
        requestTimeout: 60000
    })    
    return client.MergeRequests.all({projectId: argv.gitlab_projectId, state: 'merged', order_by: 'updated_at', perPage: 100, maxPages: 1})
}

function commitIsTagged(sha) {
    var tagged = tagCommits[sha]
    return tagged 
}

function filterMRReleased(mergeRequests) {
    console.log('Fetched MRs: ',  mergeRequests.length)
    var recentMRtagged = null
    var mrNotReleased = (mr) => {
        if (mr.target_branch != currentBranch)
            return false
        var sha = mr.merge_commit_sha || mr.sha
        var tagged = commitIsTagged(sha)
        if (tagged && !recentMRtagged)
            recentMRtagged = tagged
        console.log('Looking tags for: ', mr.iid, sha, tagged != null)
        if (tagged) 
            return false
        if (recentMRtagged && recentMRtagged.commit.id != sha) {
            // filter out MR ancestors of released
            
            var recentMRtaggedContainsCommit = isAncestor(sha, recentMRtagged.commit)
            return recentMRtaggedContainsCommit
            // return Git.Graph.descendantOf(repo, recentMRtagged, sha).then((result) => {
            //     console.log(recentMRtagged, 'descendantOf', mr.iid, sha, ':', result)
            //     return !result
            // });
        }
        return !tagged
    }
    return Promise.filter(mergeRequests, mrNotReleased)
}

function isAncestor(sha, taggedCommit) {
    var ancestor = taggedCommit.parents[0]
    while (ancestor != null) {
        if (ancestor.id == sha)
            return true
        ancestor = ancestor.parents[0] || null
    }
    return false
}

function buildSemanticReleaseNotes(mergeRequests) {
    mergeRequests.forEach((mr) => console.log('MR:', mr.iid, mr.merge_commit_sha || mr.sha, mr.title));
    semanticReleaseNotes.build(mergeRequests, argv)
}

function publish() {

}

function openRepository() {
    repo = git(argv.git_repoPath)
    return Promise.promisify(repo.tags, {context: repo})()
        .then(tags => {
            //console.log('tags: ', tags)
            tags.map((t) => {
                if (t.name.match(tagRegex)) {
                    console.log('tag:', t.name, t.commit.id)
                    tagCommits[t.commit.id] = t
                }
            })
            //console.log('tagCommits:', tagCommits)
            return repo
        })
}

openRepository()
    .then(getMergeRequests)
    .then(filterMRReleased)
    .then(buildSemanticReleaseNotes)
    // .then(publish)
    .catch((e) => console.error(e));
